var canvas = document.getElementById('gameCanvas');
var context = canvas.getContext('2d');

var m_canvas = document.createElement('canvas');
m_canvas.width = 800;
m_canvas.height = 800;
var m_context = m_canvas.getContext('2d');


var square_width = 64;
var square_height = 64;
var grid_width = 40;
var grid_height = 20;
var SPEED = 4
canvas.width = 800;
canvas.height = 800;

var firstTile = new Image();
tile.src = "tile.jpg";
var secondTile = new Image();
tile2.src = "tile2.jpg";
var tile1 = new Tile(firstTile);
var tile2 = new Tile(secondTile);
var tileList = new TileList(2);
tileList.setTileArrayEntry(tile1);
tileList.setTileArrayEntry(tile2);
var tileMap = new TileMap(tileList, m_context, 64, grid_width, grid_height);
	
for (var i = 0; i < grid_width; i++) {
	for (var j = 0; j < grid_height; j++) {
		
		if(i % 2 == 0)
		{
			if(j % 2 == 0)
			{
				tileMap.setMapValue(j,i,1);
			}
			else
			{
				tileMap.setMapValue(j,i,0);
			}
		}
		else
		{
			if(j % 2 == 0)
			{
				tileMap.setMapValue(j,i,0);
			}
			else
			{
				tileMap.setMapValue(j,i,1);
			}
		}

		
	};
};

var viewPort = new ViewPort(0, 0, 12, 10, canvas.width, canvas.height, tileMap, m_context, 64);
viewPort.viewPortDraw();

function draw() {
  viewPort.viewPortDraw();
  context.drawImage(m_canvas, 0, 0);
  //requestAnimationFrame(render);
}

//render();


window.addEventListener("keypress", moveMap, true);

function moveMap(event)
{
	if(event.keyCode == 119) 
	{
		//alert(event.keyCode);
		viewPort.viewPortMove(0,-SPEED);
	}
	if(event.keyCode == 97)
	{
		//alert(event.keyCode);
		viewPort.viewPortMove(-SPEED,0);
	}	
	if(event.keyCode == 100)
	{
		//alert(event.keyCode);
		viewPort.viewPortMove(SPEED, 0);
	}
	if(event.keyCode == 115)
	{
		//alert(event.keyCode);
		viewPort.viewPortMove(0,SPEED);
	}
}

// A single location in the grid represented visually as an image
function Tile(tileImage)
{
	this.tileImage = tileImage;

	this.getTileImage = function() {
		return this.tileImage;
	};
}

// A list of all tiles
function TileList(tileListSize)
{
	this.tileArray = new Array(tileListSize);
	this.currentTileIndex = 0;

	this.getTileImage = function(tileIndex){
		var requestedTile = this.tileArray[tileIndex].getTileImage();
		return requestedTile;
	};

	this.setTileArrayEntry = function(newTile){
		this.tileArray[this.currentTileIndex] = newTile;
		this.currentTileIndex++;
	};

	this.printTileList = function(){
		for (var i = 0; i < this.tileArray.length; i++) {
			console.log(this.tileArray[i].getTileImage());
		};
	};
}

// A map of tiles contained in the tile list using a 2 dimensional array to store grid locations
function TileMap(tileList, context, tileSize, mapWidth, mapHeight)
{
	this.tileList = tileList;
	this.context = context;
	this.tileSize = tileSize;
	this.mapWidth = mapWidth;
	this.mapHeight = mapHeight;
	this.mapPixelWidth = mapWidth * tileSize;
	this.mapPixelHeight = mapHeight * tileSize;
	
	// Create a map array 
	this.map = new Array(this.mapWidth);

	// Set up the map array
	for (var i = 0; i < this.map.length; i++){
		this.map[i] = new Array(this.mapHeight);
	};

	this.getMapPixelWidth = function(){
		return this.mapPixelWidth;
	};

	this.getMapPixelHeight = function(){
		return this.mapPixelHeight;
	};

	this.getMapWidth = function(){
		return this.mapWidth;
	};

	this.getMapHeight = function(){
		return this.mapHeight;
	};

	this.setMapValue = function(row, col, tileValue){
		this.map[row][col] = tileValue;
	};

	this.getMapValue = function(row, col){
		var mapValue = this.map[row][col];
		return mapValue;
	};

	this.getMapLocationImage = function(row, col){
		var currentTileIndex = this.map[row][col];
		return this.tileList.getTileImage(currentTileIndex);
	};

	this.drawMap = function(){
		for (var c = 0; c < this.mapWidth; c++) 
		{
			for (var r = 0; r < this.mapHeight; r++) 
			{
				var index = this.map[r][c];
				var currentTile = new Image();
				currentTile.src = this.tileList.getTileImage(index);
				var x = c * tileSize;
				var y = r * tileSize;

				this.tempContext.drawImage(currentTile, y, x);
			};
		};
	};
}

// A viewport to move the view around the map so the user can traverse the world
function ViewPort(y, x, tilesWide, tilesHigh, viewPortWidth, viewPortHeight, tileMap, context, tileSize)
{
	this.viewPortWorldY = y;
	this.viewPortWorldX = x;
	this.tilesWide = tilesWide;
	this.tilesHigh = tilesHigh;
	this.viewPortWidth = viewPortWidth;
	this.viewPortHeight = viewPortHeight;
	this.tileMap = tileMap;
	this.context = context;
	this.tileSize = tileSize;

	this.viewPortMove = function(xMove, yMove){
		var newX = this.viewPortWorldX + xMove;
		var newY = this.viewPortWorldY + yMove;

		if(this.canMove(newX, newY))
		{
			this.viewPortWorldX += xMove;
			this.viewPortWorldY += yMove;
		}
	};

	this.canMove = function(newX, newY){
		var canMove = true;
		var viewPortRightEdge = newX + this.viewPortWidth;
		var viewPortBottomEdge = newY + this.viewPortHeight;
		var mapWidth = this.tileMap.getMapPixelWidth();
		var mapHeight = this.tileMap.getMapPixelHeight();

		if((viewPortRightEdge > mapWidth) || (newX < 0))
		{
			canMove = false;
		}

		if((viewPortBottomEdge > mapHeight) || (newY < 0))
		{
			canMove = false;
		}

		return canMove;
	};

	this.viewPortDraw = function(){
		var viewPortTileX = Math.round(this.viewPortWorldX/this.tileSize);
		var viewPortTileY = Math.round(this.viewPortWorldY/this.tileSize);
		var viewPortTileMaxX = viewPortTileX + (this.tilesWide + 1);
		var viewPortTileMaxY = viewPortTileY + (this.tilesHigh + 1);
		var offsetX = this.viewPortWorldX%this.tileSize;
		var offsetY = this.viewPortWorldY%this.tileSize;
		

		for (var c = viewPortTileX; c < viewPortTileMaxX; c++) {
			
			if(c != this.tileMap.getMapWidth()){
				
				for (var r = viewPortTileY; r < viewPortTileMaxY; r++) {
					
					if(r != this.tileMap.getMapHeight())
					{
						console.log("loading image: " + " col: " + c + " row: " + r);
						var tile = this.tileMap.getMapLocationImage(r,c);

						var locationX = ((c-viewPortTileX)*tileSize) - offsetX;
						var locationY = ((r-viewPortTileY)*tileSize) - offsetY;

						this.context.drawImage(tile, locationX, locationY, tileSize, tileSize);
					}
				}
			}
		}

		
	};
}



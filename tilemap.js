// A map of tiles contained in the tile list using a 2 dimensional array to store grid locations
function TileMap(tileList, context, tileSize, mapWidth, mapHeight)
{
	this.tileList = tileList;
	this.context = context;
	this.tileSize = tileSize;
	this.mapWidth = mapWidth;
	this.mapHeight = mapHeight;
	this.mapPixelWidth = mapWidth * tileSize;
	this.mapPixelHeight = mapHeight * tileSize;
	
	// Create a map array 
	this.map = new Array(this.mapHeight);

	// Set up the map array
	for (var i = 0; i < this.map.length; i++){
		this.map[i] = new Array(this.mapWidth);
	};

	this.getMapPixelWidth = function(){
		return this.mapPixelWidth;
	};

	this.getMapPixelHeight = function(){
		return this.mapPixelHeight;
	};

	this.getMapWidth = function(){
		return this.mapWidth;
	};

	this.getMapHeight = function(){
		return this.mapHeight;
	};

	this.getMap = function(){
		for (var i = 0; i < this.map.length; i++) {
			console.log(this.map[i]);
		}
	};

	this.setMapValue = function(row, col, tileValue){
		this.map[row][col] = tileValue;
	};

	this.getMapValue = function(row, col){
		var mapValue = this.map[row][col];
		return mapValue;
	};

	this.getMapLocationImage = function(row, col){
		var currentTileIndex = this.map[row][col];
		return this.tileList.getTileImage(currentTileIndex);
	};

	this.drawMap = function(){
		
		for (var r = 0; r < this.mapHeight; r++) 
		{
			for (var c = 0; c < this.mapWidth; c++) 
			{
				var index = this.map[r][c];
				var currentTile = new Image();
				currentTile.src = this.tileList.getTileImage(index);
				var x = c * tileSize;
				var y = r * tileSize;

				console.log("Row: " + r + " Col: " + c + " x: " + x + " y: " + y);
				this.context.drawImage(currentTile, y, x);
			}
		}
	};
}
var canvas = document.getElementById('gameCanvas');
var context = canvas.getContext('2d');

var bufferCanvas = document.createElement('canvas');
var bufferContext = bufferCanvas.getContext('2d');
 
<!-- MAP SETUP STARTS-->

var square_width = 64;
var square_height = 64;
var grid_width = 80;
var grid_height = 40;
var SPEED = 4;

<!-- MAP SETUP ENDS -->

<!-- GAME SETUP STARTS -->

var firstTile = new Image();
firstTile.src = "tile.jpg";
var secondTile = new Image();
secondTile.src = "tile2.jpg";
var tile1 = new Tile(firstTile);
var tile2 = new Tile(secondTile);
var tileList = new TileList(2);
tileList.setTileArrayEntry(tile1);
tileList.setTileArrayEntry(tile2);
var tileMap = new TileMap(tileList, context, 64, grid_width, grid_height);
	
for (var i = 0; i < grid_width; i++) {
	for (var j = 0; j < grid_height; j++) {
		
		if(i % 2 == 0)
		{
			if(j % 2 == 0)
			{
				tileMap.setMapValue(j,i,1);
			}
			else
			{
				tileMap.setMapValue(j,i,0);
			}
		}
		else
		{
			if(j % 2 == 0)
			{
				tileMap.setMapValue(j,i,0);
			}
			else
			{
				tileMap.setMapValue(j,i,1);
			}
		}

		//console.log("i(width) : " + i + " j(height) : " + j);
	}
}

var vptileWidth = Math.floor(canvas.width/square_width);
var vptileHeight = Math.floor(canvas.height/square_height);
var viewPort = new ViewPort(0, 0, 30, 15, canvas.width, canvas.height, tileMap, canvas, context, bufferCanvas, bufferContext, 64);

<!-- GAME SETUP ENDS -->


<!-- CANVAS SETUP STARTS -->

window.addEventListener("keypress", moveMap, true);

function moveMap(event)
{
	// W
	if(event.keyCode == 119) 
	{
		//alert(event.keyCode);
		viewPort.move(0, -SPEED);
	}

	// A
	if(event.keyCode == 97)
	{
		//alert(event.keyCode);
		viewPort.move(-SPEED, 0);
	}	

	// D
	if(event.keyCode == 100)
	{
		//alert(event.keyCode);
		viewPort.move(SPEED, 0);
	}

	// S
	if(event.keyCode == 115)
	{
		//alert(event.keyCode);
		viewPort.move(0, SPEED);
	}
	viewPort.update();
}

// listen for a window resize and update the width/height of the canvas
window.addEventListener('resize', resizeCanvas, false);

function resizeCanvas() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        bufferCanvas.width = window.innerWidth;
        bufferCanvas.height = window.innerHeight;
        var vptileWidth = Math.floor(canvas.width/square_width);
		var vptileHeight = Math.floor(canvas.height/square_height);
		var viewPort = new ViewPort(0, 0, 30, 15, canvas.width, canvas.height, tileMap, canvas, context, bufferCanvas, bufferContext, 64);
        drawStuff(viewPort);
}

// Automatically resize the canvas when the script loads
resizeCanvas();

<!-- CANVAS SETUP ENDS -->

function drawStuff(viewPort) {
	//context.fillStyle = "#333FFC";
	//context.fillRect(0,0,canvas.width, canvas.height);
	console.log(viewPort.getTilesWide());
	console.log(viewPort.getTilesHigh());
	console.log(canvas.width + " " + canvas.height);
	viewPort.drawMap();
}

// A single location in the grid represented visually as an image
function Tile(tileImage)
{
	this.tileImage = tileImage;

	this.getTileImage = function() {
		return this.tileImage;
	};
}

// A list of all tiles
function TileList(tileListSize)
{
	this.tileArray = new Array(tileListSize);
	this.currentTileIndex = 0;

	this.getTileImage = function(tileIndex){
		var requestedTile = this.tileArray[tileIndex].getTileImage();
		return requestedTile.src;
	};

	this.setTileArrayEntry = function(newTile){
		this.tileArray[this.currentTileIndex] = newTile;
		this.currentTileIndex++;
	};

	this.printTileList = function(){
		for (var i = 0; i < this.tileArray.length; i++) {
			console.log(this.tileArray[i].getTileImage());
		};
	};
}

// A map of tiles contained in the tile list using a 2 dimensional array to store grid locations
function TileMap(tileList, context, tileSize, mapWidth, mapHeight)
{
	this.tileList = tileList;
	this.context = context;
	this.tileSize = tileSize;
	this.mapWidth = mapWidth;
	this.mapHeight = mapHeight;
	this.mapPixelWidth = mapWidth * tileSize;
	this.mapPixelHeight = mapHeight * tileSize;
	
	// Create a map array 
	this.map = new Array(this.mapHeight);

	// Set up the map array
	for (var i = 0; i < this.map.length; i++){
		this.map[i] = new Array(this.mapWidth);
	};

	this.getMapPixelWidth = function(){
		return this.mapPixelWidth;
	};

	this.getMapPixelHeight = function(){
		return this.mapPixelHeight;
	};

	this.getMapWidth = function(){
		return this.mapWidth;
	};

	this.getMapHeight = function(){
		return this.mapHeight;
	};

	this.getMap = function(){
		for (var i = 0; i < this.map.length; i++) {
			console.log(this.map[i]);
		}
	};

	this.setMapValue = function(row, col, tileValue){
		this.map[row][col] = tileValue;
	};

	this.getMapValue = function(row, col){
		var mapValue = this.map[row][col];
		return mapValue;
	};

	this.getMapLocationImage = function(row, col){
		var currentTileIndex = this.map[row][col];
		return this.tileList.getTileImage(currentTileIndex);
	};

	this.drawMap = function(){
		
		for (var r = 0; r < this.mapHeight; r++) 
		{
			for (var c = 0; c < this.mapWidth; c++) 
			{
				var index = this.map[r][c];
				var currentTile = new Image();
				currentTile.src = this.tileList.getTileImage(index);
				var x = c * tileSize;
				var y = r * tileSize;

				console.log("Row: " + r + " Col: " + c + " x: " + x + " y: " + y);
				this.context.drawImage(currentTile, y, x);
			}
		}
	};
}

// A viewport to move the view around the map so the user can traverse the world
function ViewPort(x, y, tilesWide, tilesHigh, viewPortWidth, viewPortHeight, tileMap, canvas, context, bufferCanvas, bufferContext, tileSize)
{
	this.worldPos = new Point(x, y);
	this.velocity = new Point(0, 0);
	this.tilesWide = tilesWide;
	this.tilesHigh = tilesHigh;
	this.viewPortWidth = tilesWide*tileSize;
	this.viewPortHeight = tilesHigh*tileSize;
	this.tileMap = tileMap;
	this.canvas = canvas;
	this.context = context;
	this.bufferCanvas = bufferCanvas;
	this.bufferContext = bufferContext;
	this.tileSize = tileSize;

	this.getTilesWide = function(){
		return this.tilesWide;
	};

	this.getTilesHigh = function(){
		return this.tilesHigh;
	};

	this.move = function(xMove, yMove){
		var newX = this.worldPos.x + xMove;
		var newY = this.worldPos.y + yMove;

		if(this.canMove(newX, newY))
		{
			this.worldPos.x += xMove;
			this.worldPos.y += yMove;
			this.velocity.x = xMove;
			this.velocity.y = yMove;
			this.update();
		}
	};

	this.canMove = function(newX, newY){
		var canMove = true;
		var viewPortRightEdge = newX + this.viewPortWidth;
		var viewPortBottomEdge = newY + this.viewPortHeight;
		var mapWidth = this.tileMap.getMapPixelWidth();
		var mapHeight = this.tileMap.getMapPixelHeight();

		if((viewPortRightEdge > mapWidth) || (newX < 0))
		{
			canMove = false;
		}

		if((viewPortBottomEdge > mapHeight) || (newY < 0))
		{
			canMove = false;
		}

		return canMove;
	};

	this.update = function(){
		//bufferContext.clearRect(0, 0, canvas.width, canvas.height);
		//this.bufferContext.drawImage(canvas, this.velocity.x*-1, this.velocity.y*-1);
		this.drawNextChunk();
		this.velocity.x = 0;
		this.velocity.y = 0;
		
	};

	this.drawMap = function(){
		var viewPortTileX = this.worldPos.x/this.tileSize;
		var viewPortTileY = Math.round(this.worldPos.y/this.tileSize);
		var viewPortTileMaxX = viewPortTileX + (this.tilesWide + 1);
		var viewPortTileMaxY = viewPortTileY + (this.tilesHigh + 1);
		var offsetX = this.worldPos.x%this.tileSize;
		var offsetY = this.worldPos.y%this.tileSize;
		
		
		for (var r = viewPortTileY; r < viewPortTileMaxY+1; r++) {
			
			if(r != this.tileMap.getMapHeight())
			{
				for (var c = viewPortTileX; c < viewPortTileMaxX+1; c++) {
			
					if(c != this.tileMap.getMapWidth()){
				

						var tile = new Image();
						tile.src = this.tileMap.getMapLocationImage(r,c);

						var locationX = ((c-viewPortTileX)*tileSize) - offsetX;
						var locationY = ((r-viewPortTileY)*tileSize) - offsetY;

						this.bufferContext.drawImage(tile, locationX, locationY, tileSize, tileSize);
					}
				}
			}
		}

		this.context.drawImage(bufferCanvas,0,0);
		
	};

	this.drawNextChunk = function(){

		// Position to start drawing tiles
		var firstTileX; 
		var firstTileY;
		var tileX;
		var tileY;
		var startTileRow;
		var endTileRow;
		var startTileCol;
		var endTileCol;

		if(this.velocity.x > 0){
			firstTileX = (Math.floor(this.worldPos.x/this.tileSize)*this.tileSize);
			firstTileY = (Math.floor(this.worldPos.y/this.tileSize)*this.tileSize);
			tileX = firstTileX + ((this.tilesWide)*this.tileSize) - this.worldPos.x;
			tileY = firstTileY;
			startTileCol = Math.floor((this.worldPos.x + this.viewPortWidth)/this.tileSize);
			endTileCol = startTileCol + 1;
			startTileRow = Math.floor(this.worldPos.y/this.tileSize);
			endTileRow = startTileRow + this.tilesHigh;
		}
		else if(this.velocity.x < 0){
			firstTileX = (Math.floor(this.worldPos.x/this.tileSize)*this.tileSize);
			firstTileY = (Math.floor(this.worldPos.y/this.tileSize)*this.tileSize);
			tileX = 0 - (this.worldPos.x - firstTileX);
			tileY = firstTileY;
		}
		else if(this.velocity.y > 0){
			firstTileX = (Math.floor(this.worldPos.x/this.tileSize)*this.tileSize);
			firstTileY = (Math.floor(this.worldPos.y/this.tileSize)*this.tileSize);
			tileX = firstTileX;
			tileY = firstTileY + ((this.tilesHigh-1)*this.tileSize);;
		}
		else if(this.velocity.x < 0){
			firstTileX = (Math.floor(this.worldPos.x /this.tileSize)*this.tileSize);
			firstTileY = (Math.floor((this.worldPos.y + this.viewPortHeight)/this.tileSize)*this.tileSize);
			tileX = firstTileX;
			tileY = firstTileY - ((this.tilesWide+1)*this.tileSize);
		}
		else{
			return;
		}

		
		for (var i = 0; i < this.tilesHigh + 1; i++) {
			var tile = new Image();
			tile.src = this.tileMap.getMapLocationImage(i,0);
			this.bufferContext.drawImage(tile, tileX, tileY+(64*i), this.tileSize, this.tileSize);
		};
		

		console.log("First Tile X : " + firstTileX + " First Tile Y : " + firstTileY );
		console.log("Tile X : " + tileX + " Tile Y : " + tileY);
		console.log("Tiles Wide : " + tilesWide + " Tiles High : " + tilesHigh);
		
		/*
		// location on the map of the tiles to draw
		for (var r = startTileRow; r < endTileRow; r++) {
			if(r != this.tileMap.getMapHeight()){
				for (var c = startTileCol; c < endTileCol; c++) {
					if(c != this.tileMap.getMapWidth()){
						var tile = new Image();
						tile.src = this.tileMap.getMapLocationImage(r,c);


						var locationX = (tileX + (c*tileSize) - tileSize);
						var locationY = (tileY + (r*tileSize) - tileSize);

						this.bufferContext.drawImage(tile, tileX, tileY, this.tileSize, this.tileSize);
					}
				}
			}
		}
		
		*/
	};
}

// Set up a point object
function Point(x, y){
	this.x = x;
	this.y = y;
}